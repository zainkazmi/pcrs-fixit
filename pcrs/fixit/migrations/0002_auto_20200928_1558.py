# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2020-09-28 19:58
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('fixit', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentfixitprofile',
            name='is_control_group',
        ),
        migrations.RemoveField(
            model_name='studentfixitprofile',
            name='section',
        ),
        migrations.AddField(
            model_name='problemrecommendedfixit',
            name='date',
            field=models.DateField(default=datetime.date.today),
        ),
        migrations.AddField(
            model_name='studentfixitprofile',
            name='problem_id',
            field=models.IntegerField(default=9999),
        ),
        migrations.AddField(
            model_name='studentfixitprofile',
            name='problem_type',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='problem type'),
        ),
        migrations.AddField(
            model_name='studentfixitprofile',
            name='submission_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 28, 19, 58, 55, 999368, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='problemrecommendedfixit',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
